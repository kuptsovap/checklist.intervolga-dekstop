package crawlerUfos;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.url.WebURL;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.regex.Pattern;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import testApi.*;

/*
 Отдельный класс паука, который запускается контроллером в новом потоке.
 Класс:
 - обходит страницы
 - выполняет на страницах переданный набор тестов
 - возвращает в отчет о результате теста ( формат отчета - см. класс Report в пакете testAPI ) 
 */
public class Crawler extends WebCrawler {

    // Регулярки, помогающие определять тип файла
    private static final Pattern binaryFiles = Pattern.compile(".*(\\.(mid|mp2|mp3|mp4|wav|avi|mov|mpeg|ram|m4v|pdf|rm|smil|wmv|swf|wma|zip|rar|gz))$");
    private static final Pattern imageFiles = Pattern.compile(".*(\\.(bmp|gif|jpe?g|png|tiff?))$");

    // Массивы для поиска - допустимые ссылки, маски включения и исключения
    private ArrayList<String> urls;
    private ArrayList<String> includeMasks;
    private ArrayList<String> excludeMasks;

    // Список отчетов по тесту
    private HashMap<basicTestClass, ArrayList<Report>> reports;
    // Еабор выполняемых тестов
    private ArrayList<basicTestClass> tests;

    // Таблица, куда выводятся данные о ходе выполнения тестов
    private JTable table;

    // Описание-статус кода текущей ссылки
    private String currentStatusCodeDescription;

    // Отчет по-умолчанию
    private Report defaultReport;

    /*
     Метод, добавляющий в таблицу описание ссылки
     url - ссылка
     statusCode - код ответа
     statusCodeDescription - описание кода ответа
     contentType - тип контента
     depth - глубина ссылки
     length - размер контента
     */
    protected void addUrlInTable(String url, int statusCode, String statusCodeDescription, String contentType, int depth, int length) {
        // обновление таблицы с результатами выполнения тестов
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        Object rowData[] = {url, statusCode, statusCodeDescription, contentType, depth, length}; // информация о ссылке для отображения в таблицы результатов выполнения тестов
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                model.addRow(rowData); // добавление строки в таблицу
            }
        });
    }

    public Crawler(ArrayList<String> urls, ArrayList<String> includeMasks, ArrayList<String> excludeMasks, HashMap<basicTestClass, ArrayList<Report>> reports, JTable table, ArrayList<basicTestClass> tests) {

        this.urls = new ArrayList<>();
        this.includeMasks = new ArrayList<>();
        this.excludeMasks = new ArrayList<>();

        this.urls = urls;
        this.includeMasks = includeMasks;
        this.excludeMasks = excludeMasks;

        this.reports = reports;
        this.tests = tests;

        this.table = table;

        defaultReport = new Report();
    }

    /*
     Проверка на то - нужно или нет выполнять тесты на данной ссылке
     */
    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        // добавление домена в список сайтов, если его нет - например, начинаем поиск с внутреннего каталога
        if (!urls.contains("http://" + url.getDomain())) {
            boolean itsTestingSite = false;
            for (String checkedUrl : urls) {
                if (checkedUrl.contains(url.getDomain())) {
                    itsTestingSite = true;
                    break;
                }
            }
            if (itsTestingSite) {
                if (url.getSubDomain().equals("")) {
                    urls.add("http://" + url.getDomain());
                } else {
                    urls.add("http://" + url.getSubDomain() + "." + url.getDomain());
                }
            }
        }
        String link = url.getURL(); // текущая ссылка
        String parentLink = url.getParentUrl(); // предыдущая ссылка ( родитель )

        if (parentLink == null) { // если родителя нет - делаем его пустым
            parentLink = "";
        }

        boolean isGoodLink = false; // нужно ли посещать текущую ссылку

        for (String urlForCheck : urls) {
            if (link.contains(urlForCheck) || parentLink.contains(urlForCheck)) { // проверяем все допустимые ссылки
                isGoodLink = true;
            }
        }

        return isGoodLink;
    }

    /*
     Метод, запускающий тесты, обновляющий состояние теста в таблице и добавляющий их в отчет.
     */
    @Override
    public void visit(Page page) {
        String link = page.getWebURL().getURL(); // текущая ссылка
        String parentLink = page.getWebURL().getParentUrl(); // предыдущая ссылка ( родитель )

        boolean isLinkForTests = false;

        if (!includeMasks.isEmpty() && excludeMasks.isEmpty()) { // задан поиск по маске
            for (String maskForCheck : includeMasks) {
                if (link.contains(maskForCheck)) { // проверяем совпадение по маске
                    isLinkForTests = true;
                }
            }
        } else if (!includeMasks.isEmpty() && !excludeMasks.isEmpty()) { // задан поиск по маске и заданы исключения
            for (String maskForCheck : includeMasks) {
                if (link.contains(maskForCheck)) { // проверяем совпадение по маске
                    for (String exlude : excludeMasks) {
                        if (!link.contains(exlude)) { // не входит в исключение
                            isLinkForTests = true;
                        }
                    }
                }
            }
        } else if (includeMasks.isEmpty() && !excludeMasks.isEmpty()) { // заданы исключения
            for (String urlForCheck : urls) {
                if (link.contains(urlForCheck)) { // проверяем все допустимые ссылки
                    for (String exlude : excludeMasks) {
                        if (!link.contains(exlude)) { // не входит в исключение
                            isLinkForTests = true;
                        }
                    }
                }
            }
        } else {
            for (String urlForCheck : urls) {
                if (link.contains(urlForCheck)) { // проверяем все допустимые ссылки
                    isLinkForTests = true;
                }
            }
        }

        if (!isLinkForTests) {
            return;
        }

        Report testReport;

        String contentType = page.getContentType();
        String url = page.getWebURL().getURL();

        for (basicTestClass test : tests) { // запуск всех необходимых тестов и добавление их отчетов
            if (!reports.keySet().contains(test)) {
                reports.put(test, new ArrayList());
            }
            testReport = null;
            // в зависимости от типа страницы - запускаем лишь допустимые тесты
            if (contentType.contains("text/html") && (test instanceof htmlBasicTestClass || test instanceof jsBasicTestClass)) {
                testReport = test.run(page, url);
            } else if (contentType.contains("text/css") && test instanceof cssBasicTestClass) {
                testReport = test.run(page, url);
            } else if (contentType.contains("text/javascript") && test instanceof jsBasicTestClass) {
                testReport = test.run(page, url);
            } else if (contentType.contains("image") && test instanceof imageFileBasicTestClass) {
                testReport = test.run(page, url);
            } else if (url.contains(".html") && test instanceof htmlBasicTestClass || test instanceof jsBasicTestClass) {
                testReport = test.run(page, url);
            } else if (url.contains(".css") && test instanceof cssBasicTestClass) {
                testReport = test.run(page, url);
            } else if (url.contains(".js") && test instanceof jsBasicTestClass) {
                testReport = test.run(page, url);
            } else if (imageFiles.matcher(url).matches() && test instanceof imageFileBasicTestClass) {
                testReport = test.run(page, url);
            } else if (binaryFiles.matcher(url).matches() && test instanceof binaryFileBasicTestClass) {
                testReport = test.run(page, url);
            } else {
                testReport = test.run(page, url);
            }

            // если тест прошел успешно - добавляем его для данной страницы
            if (testReport != null && !testReport.equals(defaultReport)) { // проверка, что возвращается тест, где переопределено хотя бы код и имя
                testReport.setUrl(url);
                reports.get(test).add(testReport);
            }
        }

        // обновление таблицы с результатами выполнения тестов
        addUrlInTable(page.getWebURL().getURL(), page.getStatusCode(), currentStatusCodeDescription,
                page.getContentType(), page.getWebURL().getDepth(), page.getContentData().length); // информация о ссылке для отображения в таблицы результатов выполнения тестов
    }

    /*
     Методы, работающий при передаче ссылки - служит для определения статуса ссылки и обновлении таблицы тестов
     */
    @Override
    protected void handlePageStatusCode(WebURL webUrl, int statusCode, String statusDescription) {
        currentStatusCodeDescription = statusDescription;
        if (statusCode == 301 || statusCode == 302) { // если пришла ошибка о redirecte, то пробуем перейти на эту ссылку
            URL newurl;
            try {
                // подключение по ссылке
                newurl = new URL(webUrl.getURL());
                URLConnection connector = newurl.openConnection();
                // получение набора заголовков
                Map<String, List<String>> headerFields = connector.getHeaderFields();
                // если есть ссылка, куда можно перейти - добавляем ее в список ссылок для обхода
                if (headerFields.containsKey("Location")) {
                    String headerField = connector.getHeaderField("Location");
                    if (headerField != null) {
                        urls.add(headerField);
                    }
                }
            } catch (MalformedURLException ex) {
                Logger.getLogger(Crawler.class
                        .getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Crawler.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } else if (statusCode != 200) {
            addUrlInTable(webUrl.getURL(), statusCode, currentStatusCodeDescription, "invalid", webUrl.getDepth(), 0);
        }
    }
}
