package crawlerUfos;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JTable;
import testApi.Report;

/*
 Класс, контролирующий пауков - запускает их, останавливает, задает настройки поиска и начальные точки поиска
 */
public class CrawlerController {

    // Папка, куда складывается системный мусор во время исполнения приложения
    private String crawlStorageFolder = "temp";

    // Время задержки обращения к странице - стоит такое, чтобы успевали обрабатываться тяжелые страницы + вежливость к тестируемому сайту
    private int politenessDelay = 500;
    // По умолчанию говорит, что нужно смотреть все страницы ( кол-во )
    private int maxPageToFetch = -1;
    // По умолчанию говорит, что нужно смотреть все страницы ( в глубину ) 
    private int deep = -1;
    // Масимальный размер бинарного файла
    private int maxSizeBin = 1000000000;

    // Модель
    private Model model;
    // Таблица для обновления тестов
    private JTable table;
    // Кнопка старта тестирования
    private JButton start;
    // Ссылка на созданный контроллер, чтобы можно было с нею взаимодействовать
    private CrawlController controller;

    // Состояние контроллера
    private boolean working = false;

    public CrawlerController(Model model, JTable table, JButton start) throws Exception {

        this.model = model;
        this.table = table;
        this.start = start;

        CrawlConfig config = new CrawlConfig(); // конфигурация паука

        // задаем конфигурацию
        config.setCrawlStorageFolder(crawlStorageFolder);
        config.setPolitenessDelay(politenessDelay);
        if (model.getDepth() != 0) {
            config.setMaxDepthOfCrawling(model.getDepth());
        } else {
            config.setMaxDepthOfCrawling(deep);
        }
        if (model.getCountPageToFetch() != 0) {
            config.setMaxPagesToFetch(model.getCountPageToFetch());
        } else {
            config.setMaxPagesToFetch(maxPageToFetch);
        }
        config.setMaxDownloadSize(maxSizeBin);
        config.setResumableCrawling(false);
        config.setIncludeBinaryContentInCrawling(true);
        // назначаем нашу конфигурацию для поисковика страниц
        PageFetcher pageFetcher = new PageFetcher(config);
        // создаем и задаем конфигурацию для робота
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        robotstxtConfig.setEnabled(false);
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);

        controller = new CrawlController(config, pageFetcher, robotstxtServer);

        // добавляем начальные ссылки для поиска
        for (String link : model.getUrls()) {
            controller.addSeed(link);
        }

    }

    /*
     Запуск поиска без блокировки потока
     */
    public void startNonBlocking() {
        controller.startNonBlocking(new CrawlController.WebCrawlerFactory<Crawler>() {
            @Override
            public Crawler newInstance() throws Exception {
                return new Crawler(model.getUrls(), model.getIncludeMasks(), model.getExcludeMasks(), model.getReports(), table, model.getTests());
            }
        }, model.getCountThreads());
        start.setEnabled(false);
        working = true;
    }

    /*
     "Мягкая" остановка потока - не завершается моментально, а закрывает все созданные потоки, на это ему нужно время
     */
    public void stop() {
        // передаем контроллеру сигнал о прекращение поиска
        controller.shutdown();
        controller.waitUntilFinish();
        start.setEnabled(true);
        working = false;
    }

    /*
     Метод, возвращающий набор отчетов для заданной ссылки
     url - ссылка
     */
    public ArrayList<Report> getReportsForUrl(String url) {
        return model.getReportsForUrl(url);
    }

    /*
     Метод, показывающий состояние контроллера
     */
    public boolean isWorking() {
        return working;
    }
}
