package crawlerUfos;

import java.awt.Component;
import java.awt.Desktop;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import testApi.*;

/**
 * Главный класс, который запускает приложение. Тут же происходит настройка и
 * запуск тестирования.
 */
public class CrawlerUFOs extends javax.swing.JFrame {

    Pattern validUrl = Pattern.compile("\\b(https?|ftp|file)://[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|]", Pattern.CASE_INSENSITIVE);

    // Модель для хранения настроек и результатов работы
    private Model model;

    // Невалидные ссылки
    private ArrayList<String> invalidUrls;

    // Контроллер пауков
    private CrawlerController controller;

    // Окно с результатами выполнения тестов
    private TableTests testTable;

    // Каталог, где хранятся тестов
    private File dir;

    // Набор доступных тестов
    private ArrayList<basicTestClass> tests;

    /*
     Метод для добавления записи в список.
     List - список, в который нужно добавить запись
     Text - текст, который необходимо добавить в список
     */
    private boolean addObjToList(JList list, String text) {
        DefaultListModel listModel = (DefaultListModel) list.getModel();

        if (text.isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Отсутствует запись для добавления", "Невозможно добавить запись в спискок", JOptionPane.INFORMATION_MESSAGE);
            return false;
        }
        if (!listModel.isEmpty() && listModel.contains(text)) {
            JOptionPane.showMessageDialog(rootPane, "Данная запись уже присутствует в списке", "Невозможно добавить запись в спискок", JOptionPane.INFORMATION_MESSAGE);
            return false;
        }

        listModel.addElement(text);

        return true;
    }

    /*
     Метод для очистки поля ввода
     textField - поле, которое необходимо очистить
     */
    private void cleanField(JTextField textField) {
        textField.setText("");
    }

    /*
     Метод для удаления записи из списка.
     List - список, из которого нужно удалить запись
     */
    private boolean deleteObjFromList(JList list) {
        DefaultListModel listModel = (DefaultListModel) list.getModel();

        int pos = list.getSelectedIndex();

        if (listModel.isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Список пустой", "Невозможно удалить запись из списка", JOptionPane.INFORMATION_MESSAGE);
            return false;
        }
        if (pos == -1) {
            JOptionPane.showMessageDialog(rootPane, "Вы не выбрали запись для удаления", "Невозможно удалить запись из списка", JOptionPane.INFORMATION_MESSAGE);
            return false;
        }

        listModel.remove(pos);

        return true;
    }

    /*
     Метод для генерации html отчета на основе существуюших отчетов
     */
    private String reportsToFile() {
        StringBuilder bilder = new StringBuilder();
        bilder.append("");
        ArrayList<Report> reportsForTest = new ArrayList<>();
        boolean hasError = false;
        // составляем отчет
        bilder.append("<!DOCTYPE html>\n"
                + "<html>\n"
                + "   <head>\n"
                + "      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n"
                + "      <title>Отчет о выполненных тестах в программе CrawlerUFos</title>\n"
                + "   </head>\n"
                + "   <body>\n"
                + "     <h1>Отчет о пройденных тестах в программе CrawlersUFOs: </h1>\n ");
        int testIndex = 0;
        for (basicTestClass test : model.getTests()) {
            hasError = false;
            bilder.append("<h2>" + test.getNameTest() + "</h2>\n");
            for (Report reportTest : model.getReports().get(test)) {
                if (!reportTest.getStatus()) {
                    hasError = true;
                    reportsForTest.add(reportTest);
                }

            }
            testIndex++;
            if (!hasError) {
                bilder.append("<p> Тест пройден успешно на всех просмотренных страницах. </p>\n");
            } else {
                bilder.append("<p> Тест не пройден. </p>\n"
                        + "<p> Список ошибок: </p>"
                        + "<ol>");
                for (Report rep : reportsForTest) {
                    bilder.append("<li> Тест провалился на странице: <a href=\"" + rep.getUrl() + "\">" + rep.getUrl() + "</a></br>"
                            + " Список ошибок на странице:  <ul>");
                    for (String error : rep.getDetails()) {
                        bilder.append("<li>" + error + "</li>\n");
                    }
                    bilder.append("</ul>\n"
                            + "</li>\n");

                }
                bilder.append("</ol>\n");
                reportsForTest.clear();
            }
        }
        reportsForTest.clear();
        reportsForTest = null;
        bilder.append("   </body>\n" + "</html>");
        return bilder.toString();
    }

    /*
     Метод для перевода списка отчетов по конкретному тесту в один объект отчета в формате JSON
     reports - список отчетов по конкретному тесту
     */
    private String reportsToJSONObject(ArrayList<Report> reports) {
        StringBuilder builder = new StringBuilder("");
        boolean hasErrors = false;
        if (reports == null || reports.isEmpty()) {
            return "";
        }
        for (Report report : reports) {
            if (!report.getStatus()) {
                hasErrors = true;
            }
        }
        Report result = new Report();
        for (Report report : reports) {
            if (hasErrors && report.getStatus() == false) {
                result = report;
                break;
            } else if (!hasErrors && report.getStatus() == true) {
                result = report;
                break;
            } else {
                result = report;
            }
        }
        builder.append("{ \"code\" : \"").append(result.getCode()).append("\",");
        builder.append("\"name\" : \"").append(result.getName()).append("\",");
        builder.append("\"description\" : \"").append(result.getDescription()).append("\",");
        if (!hasErrors) {
            builder.append("\"status\" : \"Y\",");
            builder.append("\"details\" : \"\",");
            builder.append("\"preview\" : \"").append(result.getPreview()).append("\" }");
        } else {
            int count = 0;
            builder.append("\"status\" : \"N\",");
            if (result.getDetails() != null && !result.getDetails().isEmpty()) {
                builder.append("\"details\" : \"<ol>");
                for (Report report : reports) {
                    if (report.getDetails().isEmpty() || (report.getDetails().size() == 1 && report.getDetails().get(0).equals(report.getPreview()))) {
                        continue;
                    }
                    for (String error : report.getDetails()) {
                        builder.append("<li>").append(error).append("</li>");
                    }
                    count++;
                }
                builder.append("<ol>\",");
            }
            builder.append("\"preview\" : \"На ссылках в количестве - ").append(count).append(" шт - тест провалился\" }");
        }
        return builder.toString();
    }

    /*
     Метод, заполняющий выбранный список из переданного массива
     list - список, куда нужно занести данные
     array - массив, откуда нужно достать данные
     */
    private void updateGUIList(JList list, ArrayList<String> array) {
        list.removeAll();
        for (String el : array) {
            addObjToList(list, el);
        }
    }

    /*
     Метод, загружающий доступные тесты
     */
    private void loadTests() {
        if (!dir.exists() && !dir.isDirectory()) {
            JOptionPane.showMessageDialog(rootPane, "Не найдена папка с тестами.", "Отсутсвует папка tests", JOptionPane.WARNING_MESSAGE);
            return;
        }
        // получение списка файлов с расширением jar в папке
        File[] jars = dir.listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isFile() && file.getName().endsWith(".jar");
            }
        });
        // загруженный класс
        Class<?> maybeTest;
        // экземпляр класса
        Object test;
        // пытаемся загрузить кнайденные классы, делаем проверку на тип класса и добавляем в список
        for (File jar : jars) {
            try {
                Properties property = getTestProperty(jar); // получение настроек тестов
                if (property == null) { // настроек нет - невозможно загрузить тест
                    JOptionPane.showMessageDialog(rootPane, "В тесте " + jar.getName() + " отсутствует файл-настроек test.properties. Файл не будет загружен", "Отсутсвует файл настроек", JOptionPane.WARNING_MESSAGE);
                    continue;
                }
                String testClassName = property.getProperty("main.class"); // получение имени класса теста 
                if (testClassName == null || testClassName.length() == 0) { // если имя класса не указано
                    JOptionPane.showMessageDialog(rootPane, "В тесте " + jar.getName() + " отсутствует или неверно описание исполняемого класса."
                            + " Проверьте файл настроек test.properties на наличие строки main.class = *имя класса с пакетами через точку*. Файл не будет загружен", "Не найдена настройка главного класса", JOptionPane.WARNING_MESSAGE);
                    continue;
                }
                // загрузка класса по его имени
                URL jarURL = jar.toURI().toURL();
                URLClassLoader classLoader = new URLClassLoader(new URL[]{jarURL});
                maybeTest = classLoader.loadClass(testClassName);
                classLoader.close();
                // создание экземпляра класса
                test = maybeTest.newInstance();
                if (test instanceof basicTestClass) { // если это класс тестов - добавляем его в систему
                    tests.add((basicTestClass) test);
                } else {
                    JOptionPane.showMessageDialog(rootPane, "В архиве " + jar.getName() + " лежит не тестовый класс. Файл не будет загружен", "Ошибочный класс", JOptionPane.WARNING_MESSAGE);
                }
            } catch (IOException | ClassNotFoundException e) {
                JOptionPane.showMessageDialog(rootPane, "Ошибка: " + e.getMessage(), "Ошибка при работе с классом", JOptionPane.WARNING_MESSAGE);
            } catch (InstantiationException | IllegalAccessException ex) {
                JOptionPane.showMessageDialog(rootPane, "Ошибка: " + ex.getMessage(), "Ошибка при работе с классом", JOptionPane.WARNING_MESSAGE);
            }
        }

    }

    /*
     Метод, возвращаютщий файл-настроек тестов, если такой имеется
     file - jar-архив, который претендует на роль теста
     */
    private Properties getTestProperty(File file) throws IOException {
        Properties result = null;
        JarFile jar = new JarFile(file);
        Enumeration entries = jar.entries();

        while (entries.hasMoreElements()) {
            JarEntry entry = (JarEntry) entries.nextElement();
            if (entry.getName().equals("test.properties")) {
                InputStream is = null;
                try {
                    is = jar.getInputStream(entry);
                    result = new Properties();
                    result.load(is);
                } finally {
                    if (is != null) {
                        is.close();
                    }
                }
            }
        }
        return result;
    }

    /*
     Метод, обновляющий список доступных тестов
     */
    private void updateUI() {
        if (tests.isEmpty()) {
            addObjToList(testList, "Не подключено ни одного плагина");
        }
        for (basicTestClass test : tests) {
            addObjToList(testList, test.getNameTest());
        }
    }

    /*
     Общий метод, служащий для добавления в массив элеентов из списка и тектового поля
     field - текстовое поле, откуда берется значение
     list - список, откуда берутся значения
     array - массив, куда заносятся значения
     */
    private void updateModelField(JTextField field, JList list, ArrayList<String> array) {
        if (field == null || list == null || array == null) { // неверные входные данные
            return;
        }
        if (!field.getText().equals("")) { // поле не пустое
            array.add(field.getText());
        }
        DefaultListModel listModel = (DefaultListModel) list.getModel();
        if (!listModel.isEmpty()) { // список не пустой
            for (int i = 0; i < listModel.getSize(); i++) {
                array.add((String) listModel.get(i));
            }
        }
    }

    /**
     * Создание приложения
     */
    public CrawlerUFOs() {
        initComponents();
        model = new Model();
        tests = new ArrayList<>();
        invalidUrls = new ArrayList();
        dir = new File(".\\tests\\");
        testList.setCellRenderer(new CheckboxListCellRenderer());
        loadTests();
        updateUI();
    }

    /*
     Класс-расширение для отображения флажка-переключателя в списке доступных тестов
     */
    public class CheckboxListCellRenderer extends JCheckBox implements ListCellRenderer {

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index,
                boolean isSelected, boolean cellHasFocus) {

            setComponentOrientation(list.getComponentOrientation());
            setFont(list.getFont());
            setBackground(list.getBackground());
            setForeground(list.getForeground());
            setSelected(isSelected);
            setEnabled(list.isEnabled());

            setText(value == null ? "" : value.toString());

            return this;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel8 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        searchDepth = new javax.swing.JSpinner();
        jLabel16 = new javax.swing.JLabel();
        countPage = new javax.swing.JSpinner();
        jLabel17 = new javax.swing.JLabel();
        countThreads = new javax.swing.JSpinner();
        start = new javax.swing.JButton();
        tabPane = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        url = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        listUrls = new javax.swing.JList();
        addUrlForList = new javax.swing.JButton();
        deleteUrl = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        includeMask = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        listIncludeMasks = new javax.swing.JList();
        addIncludeMaskForList = new javax.swing.JButton();
        deleteIncludeMask = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        excludeMask = new javax.swing.JTextField();
        jScrollPane7 = new javax.swing.JScrollPane();
        listExcludeMasks = new javax.swing.JList();
        addExludeMaskForList = new javax.swing.JButton();
        deleteExludeMask = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        testList = new javax.swing.JList();
        jLabel5 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        file = new javax.swing.JMenu();
        openLastTesting = new javax.swing.JMenuItem();
        lookReportInHtml = new javax.swing.JMenuItem();
        sendReportToServer = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("CrawlerUFOs");
        setBackground(new java.awt.Color(255, 255, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        setForeground(java.awt.Color.white);
        setMinimumSize(new java.awt.Dimension(690, 400));
        setPreferredSize(new java.awt.Dimension(750, 650));

        jLabel15.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel15.setText("Введите глубину поиска:");

        searchDepth.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        searchDepth.setModel(new javax.swing.SpinnerNumberModel());
        searchDepth.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                searchDepthStateChanged(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel16.setText("Введите количество предпочтительных потоков:");

        countPage.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        countPage.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(0), Integer.valueOf(0), null, Integer.valueOf(10)));
        countPage.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                countPageStateChanged(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel17.setText("Введите максимальное количество страниц для поиска:");

        countThreads.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        countThreads.setModel(new javax.swing.SpinnerNumberModel(10, 10, 150, 10));
        countThreads.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                countThreadsStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(jLabel16))
                        .addGap(56, 56, 56)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(countThreads, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                            .addComponent(searchDepth)))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(countPage)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchDepth)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(countThreads))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(countPage))
                .addContainerGap())
        );

        start.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        start.setText("Начать тестирование");
        start.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startActionPerformed(evt);
            }
        });

        tabPane.setToolTipText("");

        jLabel1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel1.setText("Введите URL для проверки: ");

        jLabel2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel2.setText("Список провряемых URL-ов:");

        url.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                urlActionPerformed(evt);
            }
        });

        DefaultListModel<String> model = new DefaultListModel<>();
        listUrls.setModel(model);
        jScrollPane1.setViewportView(listUrls);

        addUrlForList.setText("Добавить URL");
        addUrlForList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addUrlForListActionPerformed(evt);
            }
        });

        deleteUrl.setText("Удалить URL");
        deleteUrl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteUrlActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE)
                    .addComponent(url))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(deleteUrl, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addUrlForList, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(url)
                    .addComponent(addUrlForList))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(deleteUrl))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        tabPane.addTab("Добавление ссылок", jPanel1);

        jLabel3.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel3.setText("Маска включения:");
        jLabel3.setMaximumSize(new java.awt.Dimension(187, 17));
        jLabel3.setMinimumSize(new java.awt.Dimension(187, 17));

        jLabel4.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel4.setText("Список масок вкл:");

        includeMask.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                includeMaskActionPerformed(evt);
            }
        });

        model = new DefaultListModel<>();
        listIncludeMasks.setModel(model);
        jScrollPane2.setViewportView(listIncludeMasks);

        addIncludeMaskForList.setText("Добавить маску");
        addIncludeMaskForList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addIncludeMaskForListActionPerformed(evt);
            }
        });

        deleteIncludeMask.setText("Удалить маску");
        deleteIncludeMask.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteIncludeMaskActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addComponent(includeMask))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(deleteIncludeMask, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(addIncludeMaskForList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(includeMask)
                    .addComponent(addIncludeMaskForList))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(deleteIncludeMask))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jLabel13.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel13.setText("Маска исключения:");
        jLabel13.setMaximumSize(new java.awt.Dimension(187, 17));
        jLabel13.setMinimumSize(new java.awt.Dimension(187, 17));

        jLabel14.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel14.setText("Список масок искл:");

        excludeMask.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                excludeMaskActionPerformed(evt);
            }
        });

        model = new DefaultListModel<>();
        listExcludeMasks.setModel(model);
        jScrollPane7.setViewportView(listExcludeMasks);

        addExludeMaskForList.setText("Добавить маску");
        addExludeMaskForList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addExludeMaskForListActionPerformed(evt);
            }
        });

        deleteExludeMask.setText("Удалить маску");
        deleteExludeMask.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteExludeMaskActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
                    .addComponent(excludeMask))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(addExludeMaskForList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(deleteExludeMask, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(excludeMask)
                    .addComponent(addExludeMaskForList))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(deleteExludeMask))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabPane.addTab("Добавление масок", jPanel3);

        jPanel9.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jLabel18.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel18.setText("Список доступных тестов:");

        testList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        model = new DefaultListModel<>();
        testList.setModel(model);
        jScrollPane3.setViewportView(testList);

        jLabel5.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel5.setText("Для множественного выделения или удаления выделения нужно держать нажатой клавишу Ctrl.");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addComponent(jLabel5))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPane3)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE))
        );

        tabPane.addTab("Список доступных тестов", jPanel9);

        file.setText("Файл");

        openLastTesting.setText("Открыть последнее тестирование");
        openLastTesting.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openLastTestingActionPerformed(evt);
            }
        });
        file.add(openLastTesting);

        lookReportInHtml.setText("Показать отчет в виде html");
        lookReportInHtml.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lookReportInHtmlActionPerformed(evt);
            }
        });
        file.add(lookReportInHtml);

        sendReportToServer.setText("Послать отчет на серверный модуль");
        sendReportToServer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendReportToServerActionPerformed(evt);
            }
        });
        file.add(sendReportToServer);

        jMenuBar1.add(file);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabPane)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(start, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(tabPane)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(start)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void startActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startActionPerformed
        // обновляем списки ссылок и масок
        model.clearUrls();
        updateModelField(url, listUrls, model.getUrls());
        for (String invalidUrl : model.getUrls()) {
            if (!validUrl.matcher(invalidUrl).matches()) {
                invalidUrls.add(invalidUrl);
                JOptionPane.showMessageDialog(rootPane, invalidUrl + " данная строка не является допустимым URLом.", "Неверный формат URL", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        if (!invalidUrls.isEmpty()) {
            model.getUrls().removeAll(invalidUrls);
            cleanField(url);
            updateGUIList(listUrls, model.getUrls());
        }
        invalidUrls.clear();
        model.clearIncludeMasks();
        updateModelField(includeMask, listIncludeMasks, model.getIncludeMasks());
        model.clearExcludeMasks();
        updateModelField(excludeMask, listExcludeMasks, model.getExcludeMasks());
        model.clearReports();
        // если в модели нет ни одной ссылки для тестирования, то выдаем сообщение и прекращаем запуск
        if (model.getUrls().isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Не указано ни одного URL-а для поиска", "Невозможно начать тестирование", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        try {
            // создание контроллера 
            testTable = null;
            testTable = new TableTests();
            if (!tests.isEmpty()) {
                int[] selectedIndices = testList.getSelectedIndices();
                model.clearTests();
                for (int selectedIndice : selectedIndices) {
                    model.addTest(tests.get(selectedIndice));

                }
            }
            controller = null;
            controller = new CrawlerController(model, testTable.getTable(), start);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(rootPane, "Произошла ошибка в работе приложения - " + ex.getMessage() + ". Попробуйте его перезапустить", "Произошел системный сбой", JOptionPane.WARNING_MESSAGE);
            return;
        }
        // запуск пауков и отображение результатов
        testTable.setController(controller);
        controller.startNonBlocking();
        testTable.setVisible(true);
    }//GEN-LAST:event_startActionPerformed

    private void deleteUrlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteUrlActionPerformed
        if (deleteObjFromList(listUrls)) {
            model.deleteUrl(url.getText());
        }
    }//GEN-LAST:event_deleteUrlActionPerformed

    private void addUrlForListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addUrlForListActionPerformed
        String urlText = url.getText();
        if (addObjToList(listUrls, urlText)) {
            model.addUrl(urlText);
        }
        cleanField(url);
    }//GEN-LAST:event_addUrlForListActionPerformed

    private void urlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_urlActionPerformed

    }//GEN-LAST:event_urlActionPerformed

    private void deleteIncludeMaskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteIncludeMaskActionPerformed
        if (deleteObjFromList(listIncludeMasks)) {
            model.deleteIncludeMask(includeMask.getText());
        }
    }//GEN-LAST:event_deleteIncludeMaskActionPerformed

    private void addIncludeMaskForListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addIncludeMaskForListActionPerformed
        String includeMaskText = includeMask.getText();
        if (addObjToList(listIncludeMasks, includeMaskText)) {
            model.addIncludeMask(includeMaskText);
        }
        cleanField(includeMask);
    }//GEN-LAST:event_addIncludeMaskForListActionPerformed

    private void includeMaskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_includeMaskActionPerformed

    }//GEN-LAST:event_includeMaskActionPerformed

    private void deleteExludeMaskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteExludeMaskActionPerformed
        if (deleteObjFromList(listExcludeMasks)) {
            model.deleteExcludeMask(excludeMask.getText());
        }
    }//GEN-LAST:event_deleteExludeMaskActionPerformed

    private void addExludeMaskForListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addExludeMaskForListActionPerformed
        String excludeMaskText = excludeMask.getText();
        if (addObjToList(listExcludeMasks, excludeMaskText)) {
            model.addExcludeMask(excludeMaskText);
        }
        cleanField(excludeMask);
    }//GEN-LAST:event_addExludeMaskForListActionPerformed

    private void excludeMaskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_excludeMaskActionPerformed

    }//GEN-LAST:event_excludeMaskActionPerformed

    private void searchDepthStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_searchDepthStateChanged
        model.setDepth((int) searchDepth.getValue());
    }//GEN-LAST:event_searchDepthStateChanged

    private void countPageStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_countPageStateChanged
        model.setCountPageToFetch((int) countPage.getValue());
    }//GEN-LAST:event_countPageStateChanged

    private void countThreadsStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_countThreadsStateChanged
        model.setCountThreads((int) countThreads.getValue());
    }//GEN-LAST:event_countThreadsStateChanged

    private void lookReportInHtmlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lookReportInHtmlActionPerformed
        if (model.getReports().isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Отсутсвуют отчеты о тестировании", "Нет отчетов тестирования", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        // создаем,настраиваем и открываем "выборщик" файлов
        JFileChooser fileopen = new JFileChooser();
        fileopen.setDialogTitle("Выберите файл, куда необходимо сохранить отчет");
        fileopen.setSelectedFile(new File("repotr.html")); // стандартное имя файла
        fileopen.removeChoosableFileFilter(fileopen.getAcceptAllFileFilter());
        fileopen.addChoosableFileFilter(new javax.swing.filechooser.FileFilter() { // задаем фильтр-файлов на html

            @Override
            public boolean accept(File f) {
                return f.isDirectory() || f.getName().toLowerCase().endsWith(".html");
            }

            @Override
            public String getDescription() {
                return ".html";
            }
        });
        FileWriter fw = null;
        BufferedWriter bw = null;
        int ret = fileopen.showDialog(null, "Сохранить в файл");
        if (ret == JFileChooser.APPROVE_OPTION) { // если файл выбран записываем в него информацию
            File file = fileopen.getSelectedFile();
            try {
                fw = new FileWriter(file);
                bw = new BufferedWriter(fw);
                bw.write(reportsToFile());
                // открываем файл в браузере
                Desktop desktop = Desktop.getDesktop();
                desktop.browse(file.toURI());
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(rootPane, "В системе произошел сбой при работе с файлом."
                        + " Попробуйте повторить операцию позже", "Сбой при работе с файлом", JOptionPane.WARNING_MESSAGE);
            } finally {
                try {
                    bw.close();
                    fw.close();
                } catch (IOException ex) {
                }
            }
        }
    }//GEN-LAST:event_lookReportInHtmlActionPerformed

    private void sendReportToServerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendReportToServerActionPerformed
        if (testTable == null || model.getReports().values().isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Нету отчетов для отправки на сервер", "Тестирование не проводилось", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        InputForm form = new InputForm();
        int res = JOptionPane.showConfirmDialog(this, form, "Введите данные",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.DEFAULT_OPTION);
        if (res != JOptionPane.OK_OPTION) {
            return;
        }
        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            if (!validUrl.matcher(form.getUrl()).matches()) {
                JOptionPane.showMessageDialog(rootPane, "Введен некорректный Url", "Некорректный Url", JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            if (form.getLogin().equals("") || form.getPassword().equals("")) {
                JOptionPane.showMessageDialog(rootPane, "Пустые поля недопустимы", "Поля не заполнены", JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            HttpPost post = new HttpPost(form.getUrl() + "/bitrix/tools/intervolga.checklist/crawler.php");
            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            urlParameters.add(new BasicNameValuePair("login", form.getLogin()));
            urlParameters.add(new BasicNameValuePair("password", form.getPassword()));
            ArrayList<basicTestClass> forSend = model.getTests();
            for (basicTestClass test : forSend) {
                String report = reportsToJSONObject(model.getReports().get(test));
                if (report != "") {
                    urlParameters.add(new BasicNameValuePair("reports[]", new String(report.getBytes("UTF-8"), "ISO-8859-1")));
                }
            }
            HttpResponse response = null;
            post.setEntity(new UrlEncodedFormEntity(urlParameters));
            response = httpClient.execute(post);
            if (response.getStatusLine().getStatusCode() == 200) {
                HttpEntity respEntity = response.getEntity();
                if (respEntity != null) {
                    String message = EntityUtils.toString(respEntity);
                    if (message.equals("\nОтчет успешно добавлен на сайт")) {
                        message = " Отчет добавлен в <a href=\"" + form.getUrl() + "/bitrix/admin/checklist.php?lang=ru\">Монитор Качества</a>";
                    }
                    JEditorPane jep = new JEditorPane("text/html", message);
                    jep.setEditable(false);
                    jep.setOpaque(false);
                    jep.addHyperlinkListener(new HyperlinkListener() {
                        @Override
                        public void hyperlinkUpdate(HyperlinkEvent e) {
                            if (HyperlinkEvent.EventType.ACTIVATED.equals(e.getEventType())) {
                                try {
                                    Desktop.getDesktop().browse(e.getURL().toURI());
                                } catch (URISyntaxException | IOException e2) {
                                    e2.printStackTrace();
                                }
                            }
                        }

                    });
                    JOptionPane.showMessageDialog(this, jep, "Ответ сервера", JOptionPane.INFORMATION_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(rootPane, "Ваш отчет не отправлен", "Отчет не доставлен", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (IOException ex) {
            Logger.getLogger(CrawlerUFOs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_sendReportToServerActionPerformed

    private void openLastTestingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openLastTestingActionPerformed
        if (testTable == null) { // ни одного тестирования не производилось
            JOptionPane.showMessageDialog(rootPane, "Тестирование еще не запускалось", "Нет таблицы тестирования", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        if (testTable.isVisible()) { // идет тестирование или таблица уже открыта
            JOptionPane.showMessageDialog(rootPane, "Таблица с результатами тестирования уже открыта", "Таблица тестирования уже открыта", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        testTable.setVisible(true);
    }//GEN-LAST:event_openLastTestingActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CrawlerUFOs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CrawlerUFOs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CrawlerUFOs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CrawlerUFOs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (Exception evt) {
                }
                new CrawlerUFOs().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addExludeMaskForList;
    private javax.swing.JButton addIncludeMaskForList;
    private javax.swing.JButton addUrlForList;
    private javax.swing.JSpinner countPage;
    private javax.swing.JSpinner countThreads;
    private javax.swing.JButton deleteExludeMask;
    private javax.swing.JButton deleteIncludeMask;
    private javax.swing.JButton deleteUrl;
    private javax.swing.JTextField excludeMask;
    private javax.swing.JMenu file;
    private javax.swing.JTextField includeMask;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JList listExcludeMasks;
    private javax.swing.JList listIncludeMasks;
    private javax.swing.JList listUrls;
    private javax.swing.JMenuItem lookReportInHtml;
    private javax.swing.JMenuItem openLastTesting;
    private javax.swing.JSpinner searchDepth;
    private javax.swing.JMenuItem sendReportToServer;
    private javax.swing.JButton start;
    private javax.swing.JTabbedPane tabPane;
    private javax.swing.JList testList;
    private javax.swing.JTextField url;
    // End of variables declaration//GEN-END:variables
}
