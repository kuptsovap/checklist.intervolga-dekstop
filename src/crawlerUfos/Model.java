package crawlerUfos;

import java.util.ArrayList;
import java.util.HashMap;
import testApi.*;

/**
 * Модель приложения - хранит настройки системы, тесты и резултаты их работы
 */
public class Model {

    // Массивы для поиска - допустимые ссылки, маски включения и исключения
    private ArrayList<String> urls;
    private ArrayList<String> includeMasks;
    private ArrayList<String> excludeMasks;

    // Настройки поиска
    private int depth = 0; // Глубина
    private int countThreads = 10; // Количество пауков
    private int countPageToFetch = 0; // Количество страниц для обхода

    // Список тестов и отчетов
    private HashMap<basicTestClass, ArrayList<Report>> reports;
    private ArrayList<basicTestClass> tests;

    public Model() {
        urls = new ArrayList<String>();
        includeMasks = new ArrayList<String>();
        excludeMasks = new ArrayList<String>();
        reports = new HashMap<>();
        tests = new ArrayList<basicTestClass>();
    }

    // Ниже методы для добавления и получения свойств
    public ArrayList<basicTestClass> getTests() {
        return tests;
    }

    public HashMap<basicTestClass, ArrayList<Report>> getReports() {
        return reports;
    }

    public ArrayList<Report> getReportsForUrl(String url) {
        ArrayList<Report> result = new ArrayList<>();
        for (ArrayList<Report> reports : getReports().values()) {
            if(!reports.isEmpty()) {
                for (Report report : reports) {
                    if (report.getUrl().equals(url)) {
                        result.add(report);
                    }
                }
            }
        }
        return result;
    }

    public int getCountPageToFetch() {
        return countPageToFetch;
    }

    public ArrayList<String> getUrls() {
        return urls;
    }

    public ArrayList<String> getIncludeMasks() {
        return includeMasks;
    }

    public ArrayList<String> getExcludeMasks() {
        return excludeMasks;
    }

    public int getDepth() {
        return depth;
    }

    public int getCountThreads() {
        return countThreads;
    }

    public void setCountPageToFetch(int countPageToFetch) {
        this.countPageToFetch = countPageToFetch;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public void setCountThreads(int countThreads) {
        this.countThreads = countThreads;
    }

    public void addTest(basicTestClass test) {
        tests.add(test);
    }

    public void addUrl(String url) {
        urls.add(url);
    }

    public void addIncludeMask(String mask) {
        includeMasks.add(mask);
    }

    public void addExcludeMask(String mask) {
        excludeMasks.add(mask);
    }

    public void addReport(basicTestClass test, Report report) {
        reports.get(test).add(report);
    }

    public void deleteTest(basicTestClass delTest) {
        for (basicTestClass test : tests) {
            if (test.getClass().equals(delTest)) {
                tests.remove(test);
            }
        }
    }

    public void deleteUrl(String url) {
        urls.remove(url);
    }

    public void deleteIncludeMask(String mask) {
        includeMasks.remove(mask);
    }

    public void deleteExcludeMask(String mask) {
        excludeMasks.remove(mask);
    }

    public void clearTests() {
        tests.clear();
    }

    public void clearUrls() {
        urls.clear();
    }

    public void clearIncludeMasks() {
        includeMasks.clear();
    }

    public void clearExcludeMasks() {
        excludeMasks.clear();
    }

    public void clearReports() {
        reports.clear();
    }
}
