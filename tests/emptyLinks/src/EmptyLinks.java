
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import testApi.Report;
import testApi.basicTestClass;
import testApi.htmlBasicTestClass;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kynew
 */
public class EmptyLinks extends htmlBasicTestClass {

    @Override
    protected Report run(HtmlParseData pageHtml, String url) {
        boolean status = false;
        int count = 0;
        String p = "";
        ArrayList<String> detail = new ArrayList<>();
        String html = pageHtml.getHtml();
        if (!html.contains("href=\"#\"")) {
            p = "Ошибок не найдено";
            status = true;
        } else {
            count = html.split("href=\"#\"").length - 1;
            p = "Найдены ошибки в количестве " + count + " шт";
            detail.add("На странице " + url + " имеются ссылки вида href=\"#\" в количестве " + count + " шт");
        }
        return new Report("IV_BAD_LINK", "В коде нет плохих ссылок (href=#)",
                "В коде не используется href=#",
                status, p, detail);
    }

    @Override
    public basicTestClass newInstance() {
        return new EmptyLinks();
    }

    @Override
    public String getNameTest() {
        return "Отсутствуют ссылки вида href=\"#\"";
    }
    
}
