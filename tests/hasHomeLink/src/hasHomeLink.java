
import com.sun.corba.se.spi.orb.ParserData;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import testApi.Report;
import testApi.basicTestClass;
import testApi.htmlBasicTestClass;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kynew
 */
public class hasHomeLink extends htmlBasicTestClass {

    @Override
    protected Report run(HtmlParseData pageHtml, String url) {
        boolean status = false;
        String p = "";
        ArrayList<String> detail = new ArrayList<>();
        String html = pageHtml.getHtml();
        String domain = "";
        try {
            URL searchingURL = new URL(url);
            domain = searchingURL.getProtocol() + "://" + searchingURL.getAuthority();
        } catch (MalformedURLException ex) {
            Logger.getLogger(hasHomeLink.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (html.contains("href=\"" + domain + "/\"") || html.contains("href=\"/\"")) {
            p = "Ошибок не найдено";
            status = true;
        } else {
            p = "Найдены ошибки";
            detail.add("На странице " + url + " не имеется ссылки на главную");
        }
        return new Report("IV_LINK", "На главной странице существует ссылка на главную",
                "На главной странице существует ссылка на главную",
                status, p, detail);
    }

    @Override
    public basicTestClass newInstance() {
        return new hasHomeLink();
    }

    @Override
    public String getNameTest() {
        return "На страницах существует ссылка на главную";
    }

}
