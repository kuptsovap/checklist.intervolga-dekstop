/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package haslinktointervolga;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import java.util.ArrayList;
import testApi.Report;
import testApi.basicTestClass;
import testApi.htmlBasicTestClass;

/**
 *
 * @author kynew
 */
public class HasLinkToIntervolga extends htmlBasicTestClass {

    @Override
    public basicTestClass newInstance() {
        return new HasLinkToIntervolga();
    }

    @Override
    public String getNameTest() {
        return "На страницах существует ссылка на Интерволгу в единственном экземпляре";
    }

    @Override
    protected Report run(HtmlParseData pageHtml, String url) {
        boolean status = false;
        String p = "";
        ArrayList<String> detail = new ArrayList<>();
        String html = pageHtml.getHtml();
        if(html.contains("href=\"http://www.intervolga.ru/")) {
            if(html.split("href=\"http://www.intervolga.ru/").length-1 > 1) {
                p = "Найдены ошибки";
                detail.add("На странице " + url + " имеется более одной ссылки на Интерволгу");
            }
            else {
                p = "Ошибок не найдено";
                status = true;
            }
        }
        else {
            p = "Найдены ошибки";
            detail.add("На странице " + url + " не имеется ссылки на Интерволгу");
        }
        return new Report("IV_LINK_TO_INTERVOLGA","На главной странице существует ссылка на Интерволгу",
                "На главной странице существует ссылка на Интерволгу и притом в единственном экземпляре",
                status,p,detail);
    }

}
