import edu.uci.ics.crawler4j.parser.HtmlParseData;
import java.util.ArrayList;
import testApi.Report;
import testApi.basicTestClass;
import testApi.htmlBasicTestClass;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Тест на отстуствие отладочной печати на страницах сайта 
 * @author kynew
 */
public class HasntDebugPrint extends htmlBasicTestClass {

    @Override
    public basicTestClass newInstance() {
        return new HasntDebugPrint();
    }

    @Override
    public Report run(HtmlParseData page, String url) {
        Report result = null;
        boolean status = true;
        int countPre = 0;
        int countCode = 0;
        int countArray = 0;
        String prewiew = "";
        ArrayList<String> details = new ArrayList<>();
        
        String html = page.getHtml();
        
        if(html.contains("<pre")) {
            countPre = html.split("<pre").length-1;
            status = false;
            details.add("На странице " + url + " найдено использование тега pre в количестве - " + countPre + " шт");
        }
        if(html.contains("<code")) {
            countCode = html.split("<code").length-1;
            status = false;
            details.add("На странице " + url + " найдено использование тега code в количестве - " + countCode + " шт");
        }
        if(html.contains("array")) {
            countArray = html.split("array").length-1;
            status = false;
            details.add("На странице " + url + " найдено использование тега array в количестве - " + countArray + " шт");
        }
        if(status) {
            prewiew = "Ошибок не найдено";
        }
        else {
            int count = countPre + countCode + countArray;
            prewiew = "Найдены ошибки в количестве - " + count;
        }
        return new Report("IV_PAGES_DUMPS", "Удален вывод тестовых данных на главной странице", "На главной странице нет тегов pre, code, не выводится слово array", status, prewiew, details);
    }

    @Override
    public String getNameTest() {
        return "Провкерка наличия отладочной печати на страницах сайта";
    }
 
}
