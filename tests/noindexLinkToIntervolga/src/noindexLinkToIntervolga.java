
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import testApi.Report;
import testApi.basicTestClass;
import testApi.htmlBasicTestClass;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kynew
 */
public class noindexLinkToIntervolga extends htmlBasicTestClass {

    @Override
    protected Report run(HtmlParseData pageHtml, String url) {
        String domain = "";
        String domain1 = "";
        try {
            URL searchingURL = new URL(url);
            domain1 = searchingURL.getProtocol() + "://" + searchingURL.getAuthority();
            domain = searchingURL.getProtocol() + "://" + searchingURL.getAuthority() + "/";
        } catch (MalformedURLException ex) {
        }
        if (url.equals(domain) || url.equals(domain1)) {
            return new Report();
        }
        boolean status = false;
        String p = "";
        ArrayList<String> detail = new ArrayList<>();
        String html = pageHtml.getHtml();
        Pattern linkNoindex = Pattern.compile("<a href=\"http://www.intervolga.ru/(.*?)rel=\"nofollow\"");
        if (html.contains("href=\"http://www.intervolga.ru/")) {
            if (linkNoindex.matcher(html).find()) {
                status = true;
                p = "Ошибок не найдено";
            } else {
                p = "Найдены ошибки";
                detail.add("На странице " + url + " ссылка на Интерволгу не имеет атрибута rel=nofollow");
            }
        } else {
            p = "Найдены ошибки";
            detail.add("На странице " + url + " не имеется ссылки на Интерволгу");
        }
        return new Report("IV_NOINDEX_LINK", "Cсылки на Интерволгу обернуты в noindex",
                "Ссылки на Интерволгу обернуты в noindex на всех страницах, кроме главной",
                status, p, detail);
    }

    @Override
    public basicTestClass newInstance() {
        return new noindexLinkToIntervolga();
    }

    @Override
    public String getNameTest() {
        return "На страницах ссылка на Интерволгу обернута в noindex";
    }

}
