/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package useconsolecommandsinjs;

import java.util.ArrayList;
import testApi.Report;
import testApi.basicTestClass;
import testApi.jsBasicTestClass;

/**
 *
 * @author kynew
 */
public class UseConsoleCommandsInJS extends jsBasicTestClass {

    @Override
    protected Report run(String js, String url) {
        String prewiew = "";
        ArrayList<String> details = new ArrayList<String>();
        boolean status = true;
        int countConsole = 0;
        if (js.contains("console.")) {
            countConsole = js.split("console.").length - 1;
            status = false;
            details.add("На странице " + url + " найдено использование команды из семейства console.* в количестве - " + countConsole + " шт в js-скриптах");
        }
        if (status) {
            prewiew = "Ошибок не найдено";
        } else {
            prewiew = "Найдены ошибки в количестве - " + countConsole;
        }
        return new Report("IV_CONSOLE_COMMANDS", "В коде отсутствует вызов команды console.log", "В коде не используется console.log", status, prewiew, details);
    }

    @Override
    public basicTestClass newInstance() {
        return new UseConsoleCommandsInJS();
    }

    @Override
    public String getNameTest() {
        return "В js-скриптах нет использования команд console.*";
    }

}
