package testApi;

import java.util.ArrayList;
import java.util.Date;

/**
 * Модель отчета по одному тесту
 */
public class Report {

    private String code; // Код теста
    private String name; // Название теста
    private String description; // краткое описание теста
    private boolean status; // Стату теста
    private String preview; // Краткое сообщение о результате теста 
    private ArrayList<String> details; // Подробный список ошибок, если имеются 

    private Date date; // Дата тестирования 
    private String url;

    /*
     Конструктор
     с - код теста
     n - имя теста 
     d - краткое описание теста
     s - статус теста
     p - описание результата выполнения теста
     */
    public Report(String c, String n, String d, boolean s, String p) {
        this();
        code = c;
        name = n;
        description = d;
        status = s;
        preview = p;
    }

    /*
     Конструктор
     с - код теста
     n - имя теста 
     d - краткое описание теста
     s - статус теста
     p - описание результата выполнения теста
     details - массив со списком ошибок, если имеется
     */
    public Report(String c, String n, String d, boolean s, String p, ArrayList details) {
        this(c, n, d, s, p);
        this.details = details;
    }

    public Report() {
        details = new ArrayList<>();
        date = new Date();
        status = false;
        code = "DEFAULT_CODE";
        name = "DEFAULT_NAME";
        description = "DEFAULT_DESCRIPTION";
        preview = "DEFAULT_PREVIEW";
        url = "";
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String getPreview() {
        return preview;
    }

    public String getName() {
        return name;
    }

    public boolean getStatus() {
        return status;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
    
    public ArrayList<String> getDetails() {
        return details;
    }

    @Override
    public boolean equals(Object obj) {
        Report otherRep = (Report) obj;
        return this.getCode().equals(otherRep.getCode()) && this.getName().equals(otherRep.getName());
    }

}
