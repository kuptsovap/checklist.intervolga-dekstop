package testApi;

import edu.uci.ics.crawler4j.crawler.Page;

/**
 * Базовый класс тестов - необходимо создать своего наследника от этого класса,
 * реализовать все базовые методы и положить файл в папку tests
 */
public abstract class basicTestClass {

    /*
     Метод для создания экземпляра класса
     */
    public abstract basicTestClass newInstance();

    /*
     Метод выполнения теста
     page - страница для тестирования
     url - ссылка на страницу тестирования
     */
    public abstract Report run(Page page, String url);

    /*
     Метод, возвращающий название теста для выбора из списка
     */
    public abstract String getNameTest();

}
