/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testApi;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.parser.BinaryParseData;

/**
 * Класс тестов для обработки бинарных файлов
 *
 * @author kynew
 */
public abstract class binaryFileBasicTestClass extends basicTestClass {

    @Override
    public Report run(Page page, String url) {
        BinaryParseData pageHtml = null;
        if (page.getParseData() instanceof BinaryParseData) {
            pageHtml = (BinaryParseData) page.getParseData();
        }
        if (pageHtml != null) {
            Report result = run(pageHtml, url);
            if (result != null) {
                return result;
            }
            return new Report();
        }
        return new Report();
    }

    /*
     Метод выполнения теста
     file - бинарный файлик
     url - ссылка на страницу тестирования
     */
    protected abstract Report run(BinaryParseData file, String url);

}
