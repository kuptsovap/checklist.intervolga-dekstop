/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testApi;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.parser.TextParseData;

/**
 * Класс тестов для обработки css-страниц
 *
 * @author kynew
 */
public abstract class cssBasicTestClass extends basicTestClass {

    @Override
    public Report run(Page page, String url) {
        TextParseData pageHtml = null;
        if (page.getParseData() instanceof TextParseData) {
            pageHtml = (TextParseData) page.getParseData();
        }
        if (pageHtml != null) {
            Report result = run(pageHtml, url);
            if (result != null) {
                return result;
            }
            return new Report();
        }
        return new Report();
    }

    /*
     Метод выполнения теста
     style - файлик стилей
     url - ссылка на страницу тестирования
     */
    protected abstract Report run(TextParseData style, String url);
}
