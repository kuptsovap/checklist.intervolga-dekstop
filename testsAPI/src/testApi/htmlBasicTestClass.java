/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testApi;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.parser.HtmlParseData;

/**
 * Класс тестов для обработки html-страничек
 *
 * @author kynew
 */
public abstract class htmlBasicTestClass extends basicTestClass {

    @Override
    public Report run(Page page, String url) {
        HtmlParseData pageHtml = null;
        if (page.getParseData() instanceof HtmlParseData) {
            pageHtml = (HtmlParseData) page.getParseData();
        }
        if (pageHtml != null) {
            Report result = run(pageHtml, url);
            if (result != null) {
                return result;
            }
            return new Report();
        }
        return new Report();
    }

    /*
     Метод выполнения теста
     pageHtml - html-страница для тестирования
     url - ссылка на страницу тестирования
     */
    protected abstract Report run(HtmlParseData pageHtml, String url);
}
