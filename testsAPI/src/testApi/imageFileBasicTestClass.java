/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testApi;

import edu.uci.ics.crawler4j.parser.BinaryParseData;

/**
 * Класс тестов для обработки изображений
 *
 * @author kynew
 */
public abstract class imageFileBasicTestClass extends binaryFileBasicTestClass {

    /*
     Метод выполнения теста
     file - бинарный файлик
     url - ссылка на страницу тестирования
     */
    protected abstract Report run(BinaryParseData file, String url);

}
