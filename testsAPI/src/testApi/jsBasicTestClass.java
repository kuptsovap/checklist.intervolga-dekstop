/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testApi;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс тестов для обработки js-файлов и скриптов
 *
 * @author kynew
 */
public abstract class jsBasicTestClass extends basicTestClass {

    static Pattern linkJS = Pattern.compile("src=\"(.*?)\">(.*?)</script>");
    static Pattern textJS = Pattern.compile("javascript\">(.*?)</script>");
    static Pattern skipingJS = Pattern.compile("data-skip-moving=\"(true|false)\">(.*?)</script>");

    @Override
    public Report run(Page page, String url) {
        StringBuilder bilder = new StringBuilder();
        HtmlParseData pageHtml = null;
        String pageJS = "";
        bilder.append(pageJS);

        if (page.getParseData() instanceof HtmlParseData) {
            pageHtml = (HtmlParseData) page.getParseData();
        }

        if (pageHtml != null) {
            String html = pageHtml.getHtml();
            Matcher mLink = linkJS.matcher(html);
            Matcher mText = textJS.matcher(html);
            Matcher mSkiping = skipingJS.matcher(html);
            while (mLink.find()) {
                String text = mLink.group();
                String subdomain = page.getWebURL().getSubDomain();
                String domain = page.getWebURL().getDomain();
                String js = new String(text.substring(text.indexOf("\"") + 1, text.indexOf("\"", text.indexOf("\"") + 1)));
                BufferedReader reader = null;
                try {
                    URL site;
                    if (js.startsWith("//")) {
                        site = new URL("http:" + js);
                    } else if (!js.startsWith("/")) {
                        site = new URL(url + js);
                    } else if (js.startsWith("http:")) {
                        site = new URL(js);
                    } else if (subdomain.equals("")) {
                        site = new URL("http://" + domain + js);
                    } else {
                        site = new URL("http://" + subdomain + "." + domain + js);
                    }

                    reader = new BufferedReader(new InputStreamReader(site.openStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        bilder.append(line + " ");
                    }
                    site = null;
                } catch (IOException ex) {
                } finally {
                    try {
                        if (reader != null) {
                            reader.close();
                        }
                    } catch (IOException ex) {
                    }
                }
            }
            while (mText.find()) {
                String text = mText.group();
                String js = new String(text.substring(text.indexOf(">") + 1, text.indexOf("<")));
                bilder.append(js + " ");
            }
            while (mSkiping.find()) {
                String text = mSkiping.group();
                String js = new String(text.substring(text.indexOf(">") + 1, text.indexOf("<")));
                bilder.append(js + " ");
            }
            mSkiping = null;
            mText = null;
            mLink = null;
            pageHtml = null;
            Report result = run(bilder.toString(), url);
            bilder = null;
            if (result != null) {
                return result;
            }
            return new Report();
        }
        return new Report();
    }

    /*
     Метод выполнения теста
     pageJS - js-скрипты для тестирования
     url - ссылка на страницу тестирования
     */
    protected abstract Report run(String pageJS, String url);
}
